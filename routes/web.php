<?php

use Illuminate\Support\Facades\Route;
use \SendGrid\Mail\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


        // // pass contents from view make
        // $view = View::make('emails.recommendation.signup', ['name' => 'mesnja']);
        // $contents = $view->render();
        // // or
        // $contents = (string) $view;

Route::get('/mailto/{toAddress}', function ($toAddress) {
    $email = new Mail();
    // Replace the email address and name with your verified sender
    $email->setFrom(
        'tanvir@appcommandos.com',
        'Tanvir Ahmed Chowdhury'
    );
    $email->setSubject('Sending with Twilio SendGrid is Fun');
    // Replace the email address and name with your recipient
    $email->addTo(
        $toAddress,
        'Stefan Richter'
    );
    $email->addContent(
        'text/html',
        '<strong>The sendgrid API isolation test is working! Thanks.</strong>'
    );
    $sendgrid = new \SendGrid("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");
    try {
        $response = $sendgrid->send($email);
        printf("Response status: %d\n\n", $response->statusCode());

        $headers = array_filter($response->headers());
        echo "Response Headers\n\n";
        foreach ($headers as $header) {
            echo '- ' . $header . "\n";
        }
    } catch (Exception $e) {
        echo '\nCaught exception: '. $e->getMessage() ."\n";
    }
    // return view('welcome');
});
